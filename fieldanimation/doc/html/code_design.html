
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Code design &#8212; FieldAnimation 0.1.2a0 documentation</title>
    <link rel="stylesheet" href="_static/classic.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="_static/graphviz.css" />
    
    <script id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <script src="_static/language_data.js"></script>
    
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Example application" href="app.html" />
    <link rel="prev" title="Tutorial" href="tutorial.html" /> 
  </head><body>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="app.html" title="Example application"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="tutorial.html" title="Tutorial"
             accesskey="P">previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="index.html">FieldAnimation 0.1.2a0 documentation</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Code design</a></li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <div class="section" id="code-design">
<h1>Code design<a class="headerlink" href="#code-design" title="Permalink to this headline">¶</a></h1>
<p>Nowadays the visualization of large numerical models is tricky due to the
high demand of graphical resources needed.
Furthermore scientist cannot take care of the efficiency of both the numerical
model and the visualization system.
Commercial applications or free ones, like <a class="reference external" href="https://visit.llnl.gov/">Visit</a>
or <a class="reference external" href="https://www.paraview.org/">Paraview</a>, are often used to visualize data
and understand their meaning. Unfortunately these applications provide
only standard visualizations tools, like quivers or streamplots,
and are slow or need datasets converted to specific formats.</p>
<p>Field Animation was written during the development of the simulation
package <a class="reference external" href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=13&amp;ved=2ahUKEwjBj_eN-rfdAhWG-6QKHdYcCSwQFjAMegQIAhAC&amp;url=http%3A%2F%2Fconference.scipy.org%2Fproceedings%2Fscipy2015%2Fpdfs%2Fnicola_creati.pdf&amp;usg=AOvVaw0pp7loiHRIb7fr2gDBpo7b11">PyGmod</a>
for fast visualization of a geodynamic deformation field.
It implements a particles tracing visualization algorithm where
particles move according to the field streamlines giving an instantaneous
picture of its pattern and line flow.
FieldAnimation has been designed to process bidimensional arrays and
uses both <a class="reference external" href="http://pyopengl.sourceforge.net/">PyOpenGL</a> and <a class="reference external" href="http://www.numpy.org">numpy</a> to take the most of the
available hardware .</p>
<p>The adoption of the OpenGL programming pipeline allows the usage of a large
number of particles (millions) and hence more detailed models.
Most of the computation is done by the GPU and it is instrumented by shaders
written in GLSL. The CPU merely takes care of the initialization of some
data structures and arrays.
The programmer must only focus on the dataset and forget about the interfaces.
The fieldanimation package consist of three modules:
<a class="reference internal" href="api/fieldanimation.html#module-fieldanimation" title="fieldanimation"><code class="xref py py-mod docutils literal notranslate"><span class="pre">fieldanimation</span></code></a>, <a class="reference internal" href="api/fieldanimation.html#module-fieldanimation.texture" title="fieldanimation.texture"><code class="xref py py-mod docutils literal notranslate"><span class="pre">fieldanimation.texture</span></code></a>
and <a class="reference internal" href="api/fieldanimation.html#module-fieldanimation.shader" title="fieldanimation.shader"><code class="xref py py-mod docutils literal notranslate"><span class="pre">fieldanimation.shader</span></code></a>.
The last two are just support modules that handle openGL textures and shader
code loading, compiling and linking. <a class="reference internal" href="api/fieldanimation.html#module-fieldanimation" title="fieldanimation"><code class="xref py py-mod docutils literal notranslate"><span class="pre">fieldanimation</span></code></a>
is the core module in charge of arranging all the data,
set the right parameters to the OpenGL context for rendering the scene and
defines the drawing workflow.
The animated image instance creation is straightforward:</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="n">animated_image</span> <span class="o">=</span> <span class="n">FieldAnimation</span><span class="p">(</span><span class="n">width</span><span class="p">,</span> <span class="n">height</span><span class="p">,</span> <span class="n">field</span><span class="p">)</span>
</pre></div>
</div>
<ul class="simple">
<li><p><strong>width</strong> and <strong>height</strong> are the vertical and horizontal pixels dimensions
of the window</p></li>
<li><p><strong>field</strong> is an NxMx2 <a class="reference external" href="http://www.numpy.org">numpy</a> array with the field
components</p></li>
</ul>
<p>All modules are kept as simple as possible to reduce dependencies but new
functionalities can be easily added subclassing
<a class="reference internal" href="api/fieldanimation.html#fieldanimation.FieldAnimation" title="fieldanimation.FieldAnimation"><code class="xref py py-class docutils literal notranslate"><span class="pre">fieldanimation.FieldAnimation</span></code></a>.
The package can be integrated in any windowing system.</p>
<div class="section" id="particle-tracing-algorithm">
<h2>Particle tracing algorithm<a class="headerlink" href="#particle-tracing-algorithm" title="Permalink to this headline">¶</a></h2>
<p>FieldAnimation implements a simple OpenGL sequence of stages to draw on the
screen:
a vertex shader, a compute shader and a fragment shader. OpenGL connects
these shader programs with fixed functions glue. In the drawing process the
GPU executes the shaders piping their input and output along the pipeline
until pixel will come out at the end. The vertex shader stage handles
vertex processing such as space transformation, lighting and arranges work for
next rendering stages. The fragment shader manages the stage after the
rasterization of geometric primitive and defines the color of the pixel on the
screen.</p>
<p>Particle tracing starts with the generation of an array of random particle
positions on the screen. This array is stored in an OpenGL Texture object
encoding them as colors (RGBA values). A 100 pixels x 100 pixels texture
for example can store in this way 10.000 points positions. Particle
coordinates are encoded into two bytes, RG for x and BA for y</p>
<a class="reference internal image-reference" href="_images/figure1.png"><img alt="_images/figure1.png" class="align-center" src="_images/figure1.png" style="width: 256.0px; height: 88.0px;" /></a>
<p>Each
texture pixel can therefore store 65536 distinct values for each coordinate.
The texture is passed to the GPU in a vertex shader and the the original
particles positions are retrieved from the RGBA texture using the “texture
fetched method“ in the vertex shader:</p>
<div class="highlight-GLSL notranslate"><div class="highlight"><pre><span></span><span class="cp">#version 430</span>
<span class="k">layout</span> <span class="p">(</span><span class="n">location</span> <span class="o">=</span> <span class="mo">0</span><span class="p">)</span> <span class="k">in</span> <span class="kt">float</span> <span class="n">index</span><span class="p">;</span>

<span class="k">uniform</span> <span class="kt">sampler2D</span> <span class="n">tracers</span><span class="p">;</span>
<span class="k">uniform</span> <span class="kt">float</span> <span class="n">tracersRes</span><span class="p">;</span>

<span class="c1">// Model-View-Projection matrix</span>
<span class="k">uniform</span> <span class="kt">mat4</span> <span class="n">MVP</span><span class="p">;</span>
<span class="k">uniform</span> <span class="kt">float</span> <span class="n">pointSize</span><span class="p">;</span>

<span class="k">out</span> <span class="kt">vec2</span> <span class="n">tracerPos</span><span class="p">;</span>

<span class="kt">void</span> <span class="n">main</span><span class="p">()</span> <span class="p">{</span>
    <span class="c1">// Extracts RGBA value</span>
    <span class="kt">vec4</span> <span class="n">color</span> <span class="o">=</span> <span class="n">texture</span><span class="p">(</span><span class="n">tracers</span><span class="p">,</span> <span class="kt">vec2</span><span class="p">(</span>
        <span class="n">fract</span><span class="p">(</span><span class="n">index</span> <span class="o">/</span> <span class="n">tracersRes</span><span class="p">),</span>
        <span class="n">floor</span><span class="p">(</span><span class="n">index</span> <span class="o">/</span> <span class="n">tracersRes</span><span class="p">)</span>
        <span class="o">/</span> <span class="n">tracersRes</span><span class="p">));</span>

    <span class="c1">// Decodes current tracer position from the</span>
    <span class="c1">// pixel&#39;s RGBA value (range from 0  to 1.0)</span>
    <span class="n">tracerPos</span> <span class="o">=</span> <span class="kt">vec2</span><span class="p">(</span>
        <span class="n">color</span><span class="p">.</span><span class="n">r</span> <span class="o">/</span> <span class="mf">255.0</span> <span class="o">+</span> <span class="n">color</span><span class="p">.</span><span class="n">b</span><span class="p">,</span>
        <span class="n">color</span><span class="p">.</span><span class="n">g</span> <span class="o">/</span> <span class="mf">255.0</span> <span class="o">+</span> <span class="n">color</span><span class="p">.</span><span class="n">a</span><span class="p">);</span>

    <span class="nb">gl_PointSize</span> <span class="o">=</span> <span class="n">pointSize</span><span class="p">;</span>
    <span class="nb">gl_Position</span> <span class="o">=</span> <span class="n">MVP</span> <span class="o">*</span> <span class="kt">vec4</span><span class="p">(</span>
    <span class="n">tracerPos</span><span class="p">.</span><span class="n">x</span><span class="p">,</span> <span class="n">tracerPos</span><span class="p">.</span><span class="n">y</span><span class="p">,</span> <span class="mo">0</span><span class="p">,</span> <span class="mi">1</span><span class="p">);</span>
<span class="p">}</span>
</pre></div>
</div>
<p>Decoding of particles
position from texture is implemented through an array with absolute indexes
of the particles, passed to the shaders.</p>
</div>
</div>
<div class="section" id="original-publication">
<h1>Original publication<a class="headerlink" href="#original-publication" title="Permalink to this headline">¶</a></h1>
<p>Field Animation was first published
<a class="reference external" href="https://doi.org/10.1016/j.softx.2019.02.008">here</a>.</p>
</div>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
            <p class="logo"><a href="index.html">
              <img class="logo" src="_static/ogs.png" alt="Logo"/>
            </a></p>
  <h3><a href="index.html">Table of Contents</a></h3>
  <ul>
<li><a class="reference internal" href="#">Code design</a><ul>
<li><a class="reference internal" href="#particle-tracing-algorithm">Particle tracing algorithm</a></li>
</ul>
</li>
<li><a class="reference internal" href="#original-publication">Original publication</a></li>
</ul>

<h3><a href="index.html">Table of Contents</a></h3>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="overview.html">Overview</a></li>
<li class="toctree-l1"><a class="reference internal" href="overview.html#getting-started">Getting started</a></li>
<li class="toctree-l1"><a class="reference internal" href="tutorial.html">Tutorial</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">Code design</a></li>
<li class="toctree-l1"><a class="reference internal" href="#original-publication">Original publication</a></li>
<li class="toctree-l1"><a class="reference internal" href="app.html">Example application</a></li>
<li class="toctree-l1"><a class="reference internal" href="api/fieldanimation.html"><code class="xref py py-mod docutils literal notranslate"><span class="pre">fieldanimation</span></code> package reference</a></li>
<li class="toctree-l1"><a class="reference internal" href="changes.html">Changes</a></li>
</ul>

  <h4>Previous topic</h4>
  <p class="topless"><a href="tutorial.html"
                        title="previous chapter">Tutorial</a></p>
  <h4>Next topic</h4>
  <p class="topless"><a href="app.html"
                        title="next chapter">Example application</a></p>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="_sources/code_design.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="app.html" title="Example application"
             >next</a> |</li>
        <li class="right" >
          <a href="tutorial.html" title="Tutorial"
             >previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="index.html">FieldAnimation 0.1.2a0 documentation</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Code design</a></li> 
      </ul>
    </div>
    <div class="footer" role="contentinfo">
        &#169; Copyright 2018, Nicola Creati, Roberto Vidmar.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 3.1.1.
    </div>
  </body>
</html>