:mod:`fieldanimation.examples` package
===============================================================================

:mod:`fieldanimation.examples.app`
-------------------------------------------------------------------------------
.. automodule:: fieldanimation.examples.app
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:                                                            
    :show-inheritance:

