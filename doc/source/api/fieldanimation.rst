.. include:: ../include.rst 

:mod:`fieldanimation` package reference
===============================================================================

.. inheritance-diagram:: fieldanimation.FieldAnimation

.. automodule:: fieldanimation
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`fieldanimation.shader`
-------------------------------------------------------------------------------

.. inheritance-diagram:: fieldanimation.shader.Shader

.. automodule:: fieldanimation.shader
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`fieldanimation.texture` Module
-------------------------------------------------------------------------------

.. inheritance-diagram:: fieldanimation.texture.Texture

.. automodule:: fieldanimation.texture
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

Subpackages                                                                    
-------------------------------------------------------------------------------
                                                                               
.. toctree::                                                                   
    :maxdepth: 2                                                               
                                                                               
    fieldanimation.examples
