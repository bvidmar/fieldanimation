import os
import sys
sys.path.insert(0, os.path.abspath('../..'))
import matplotlib
matplotlib.use('Agg')
import fieldanimation
project = 'FieldAnimation'
copyright = '2018, Nicola Creati, Roberto Vidmar'
author = 'Nicola Creati, Roberto Vidmar'
# Retrieve version information
exec(open(os.path.join("..", "..", "fieldanimation", "__version__.py")).read())
version = __version__
# The full version, including alpha/beta/rc tags.
for line in open(os.path.join("..", "..", "setup.py")).readlines():
    if "SUBVERSION" in line:
        exec(line)
        release = "%s.%s" % (version, SUBVERSION)
        del SUBVERSION
        break

#pygments_style = 'default'
pygments_style = 'sphinx'

extensions = [
        'sphinx.ext.autodoc',
        'sphinx.ext.viewcode',
        'sphinxcontrib.napoleon',

        'sphinx.ext.inheritance_diagram',
        'sphinx.ext.intersphinx',
        'sphinx.ext.todo',

        'sphinxcontrib.autoprogram',
        'sphinxcontrib.programoutput',
        'sphinxarg.ext',
        #'sphinxcontrib.fulltoc',
        #'sphinx.ext.imgmath',
        'sphinx.ext.mathjax', 'sphinx.ext.ifconfig', #'sphinx.ext.numfig',
        ]
# todo settings
###############
todo_include_todos = True
# inheritance_diagram settings
##############################
#inheritance_graph_attrs = dict(rankdir="TB", size='"10.0, 20.0"',
  #ratio='compress', fontsize=12)
#inheritance_node_attrs = dict(fontname='Arial', fontsize=12,
#        colorscheme='accent8', style='filled')
inheritance_node_attrs = dict(fontname='Arial', colorscheme='accent8',
  style='filled')
# Napoleon settings
###################
napoleon_google_docstring = True
napoleon_numpy_docstring = False
#napoleon_include_init_with_doc = False
napoleon_include_init_with_doc = True
#napoleon_include_private_with_doc = False
napoleon_include_private_with_doc = True
#napoleon_include_special_with_doc = False
napoleon_include_special_with_doc = True
napoleon_use_admonition_for_examples = False
napoleon_use_admonition_for_notes = False
napoleon_use_admonition_for_references = False
napoleon_use_ivar = False
napoleon_use_param = True
napoleon_use_rtype = True
napoleon_use_keyword = True

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']
#html_style = 'default.css'
exclude_patterns = []
html_theme = 'default'
html_logo = "images/ogs.png"
html_show_sourcelink = True
html_sidebars = {
        '**': ['localtoc.html',
               'globaltoc.html',
               'relations.html',
               'sourcelink.html',
               'searchbox.html',
              ]
        }

html_static_path = ['_static']
intersphinx_mapping = {
    'qcobj': ('https://bvidmar.bitbucket.io/qcobj/latest', None),
    'numpy': ('https://docs.scipy.org/doc/numpy/', None),
    'scipy': ('https://docs.scipy.org/doc/scipy/reference', None),
}

numfig = True
math_number_all = True
math_numfig = True
numfig_secnum_depth = 2
math_eqref_format = "Eq.{number}"

add_function_parentheses = True
add_module_names = True
