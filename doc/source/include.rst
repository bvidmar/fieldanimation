.. |project| replace:: FieldAnimation
.. |projectl| replace:: fieldanimation
.. |projecth| replace:: https://bitbucket.org/bvidmar/fieldanimation
.. |projectg| replace:: git+https://bitbucket.org/bvidmar/fieldanimation
.. |nick| replace:: Nicola Creati
.. |bobo| replace:: Roberto Vidmar
.. |OGS| replace:: OGS

.. _PyOpenGL: http://pyopengl.sourceforge.net/
.. _project: index.html
.. _project_home: https://bitbucket.org/ncreati/large
.. _project_pypi: https://pypi.org/project/large/
.. _project_examples: https://bitbucket.org/ncreati/large/src/master/examples
.. _nick: https://www.inogs.it/users/nicola-creati
.. _bobo: https://www.inogs.it/users/roberto-vidmar
.. _OGS: https://www.inogs.it
.. _HDF5: https://www.hdfgroup.org/solutions/hdf5/

.. _PyPi: https://pypi.org/
.. _PyPi_fa: https://test.pypi.org/project/fieldanimation/
.. _art: https://pypi.org/project/art/
.. _h5py: https://pypi.org/project/h5py
.. _mpi4py: https://pypi.org/project/mpi4py/
.. _numba: https://pypi.org/project/numba/
.. _numpy: https://pypi.org/project/numpy/
.. _Pillow: https://pypi.org/project/Pillow/
.. _Pygments: https://pypi.org/project/Pygments/
.. _petsc4py: https://pypi.org/project/petsc/
.. _pip: https://pypi.org/project/pip/
.. _psutil: https://pypi.org/project/psutil/
.. _qcobj: https://pypi.org/project/qcobj/
.. _scipy: https://pypi.org/project/scipy/
.. _setuptools: https://pypi.org/project/setuptools/
.. _CINECA: https://www.cineca.it
.. _Galileo: http://www.hpc.cineca.it/hardware/galileo
.. _SLURM: https://slurm.schedmd.com/

.. _matplotlib: https://pypi.org/project/matplotlib/
.. _PySide2: https://pypi.org/project/PySide2/
.. _PyQt5: https://pypi.org/project/PyQt5/

.. _venv: https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/
.. _ga4py: https://github.com/GlobalArrays/ga4py
.. _ini: https://en.wikipedia.org/wiki/INI_file
.. _openmpi: https://www.open-mpi.org/
.. _ga: https://hpc.pnl.gov/globalarrays/
.. _pipd: https://pip-python3.readthedocs.io/en/latest/
.. _pbs: https://learn.scientificprogramming.io/learn-to-use-pbs-pro-job-scheduler-ffd9c0ad680d
