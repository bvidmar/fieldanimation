.. include:: include.rst

Tutorial
===============================================================================

FieldAnimation in a nutshell 
-------------------------------------------------------------------------------
To use |project|_ in a real PyOpenGL_ application simply:

    * create a :class:`FieldAnimation <fieldanimation.FieldAnimation>` instance
    * call its :func:`draw <fieldanimation.FieldAnimation.draw>` method in
      the main rendering loop

An example application is included in the package at
:mod:`fieldanimation.examples.app`. This module can be executed as a script:

.. _app-ref:

.. autoprogram:: fieldanimation.examples.app:makeParser()                              
    :prog: app
