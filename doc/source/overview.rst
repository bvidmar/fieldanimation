.. include:: include.rst

Overview
===============================================================================
|project|_ is a Python package to represent vector fields through particles
that move along the flow lines of the field at a speed and color proportional       
to its modulus.                                                                
                                                                               
A background image can be shown to add information for the interpretation      
of the results.                                                                
                                                                               
An **example application** with interactive control of speed,                  
color and number of animated particles is available in the examples direcory.  
                                                        
|project|_ is written in Python and supports 3.6+

Requirements
-------------------------------------------------------------------------------
The current version of |project|_ relies on 
PyOpenGL_ and numpy_. The rendering of the OpenGL image must
be carried out by a library that handles windows, input and events like
`GLFW <http://www.glfw.org/>`_ or
`PyQt <https://riverbankcomputing.com/software/pyqt/>`_

This version of |project|_ relies on the following packages:

.. command-output:: cat ../../requirements.txt

Getting started
===============================================================================
To see how |project|_ looks like install it from PyPi_ or download / clone
it from |projecth| and install it using pip_.

For the impatient:

.. parsed-literal::

    $ pip3 install |projectl|

To run the example application install also Pillow, imgui and glfw:

.. parsed-literal::

    $ pip3 install Pillow imgui glfw
    $ python3 -m fieldanimation.examples.app -g

The source code of |project|_ is available at |projecth|. 
