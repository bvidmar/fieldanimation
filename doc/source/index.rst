.. include:: include.rst

|project|_ documentation
===============================================================================

Foreword
-------------------------------------------------------------------------------
FieldAnimation has been developed by two programmers working in the 
`Aerial Operations <http://www.inogs.it/en/content/aerial-operations>`_
group of the IRI Research Section at
`Istituto Nazionale di Oceanografia e di
Geofisica Sperimentale - OGS <http://www.inogs.it>`_.

Python is their favourite programming language since 2006.

The authors:

**Nicola Creati, Roberto Vidmar**

.. image:: images/creati.jpg
   :height: 134 px
   :width:  100 px 
.. image:: images/vidmar.jpg
   :height: 134 px
   :width:  100 px 

Contents:
-------------------------------------------------------------------------------
.. toctree::                                                                   
    :maxdepth: 2                                                               
    :titlesonly:                                                               
                                                                               
    overview                                                                   
    tutorial                                                                   
    code_design
    app
    api/fieldanimation                                                                  
    changes  

.. warning::
   This code has been tested *only* on Linux (Ubuntu 18.04 and
   Ubuntu 20.04) but should work also on Mac and Windows.

.. warning::
   This is work in progress!

Indices and Tables
##################
* :ref:`genindex`
* :ref:`modindex`
